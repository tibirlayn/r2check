<?php

if ($_SERVER['REQUEST_URI'] == "/") include 'all/main.php';
else {
    $page = substr($_SERVER['REQUEST_URI'], 1);

    if (!preg_match('/^[A-z0-9\.jpg]{2,13}$/', $page)) {
        header('Location: https://r2check.ru/urlError');
        exit;
    }
    if (file_exists('all/'.$page.'.php')) include 'all/'.$page.'.php';
    else if (file_exists('admin/'.$page.'.php')) include 'admin/'.$page.'.php';
    else if (file_exists('error/'.$page.'.php')) include 'error/'.$page.'.php';
    else
        header('Location: https://r2check.ru/urlError');
        exit;
}

?>