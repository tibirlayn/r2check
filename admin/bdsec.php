<?php
class Table {
    function mainTable() {
        require_once 'admin/bd.php';
        $query = $pdo->query('SELECT `project`, `name`, `exp`, `r2drop`, `quest`, `silver`, `discord`, `vk`, `web`, `QQ`, `telegram` 
                    FROM `R2server` LEFT JOIN `R2organization` ON `R2server`.`organization_id` = `R2organization`.`id_project` 
                    WHERE `R2server`.`organization_id` = `R2organization`.`id_project` ');
        while ($row = $query->fetch(PDO::FETCH_OBJ)) {
            echo '
                <tr>
                    <td>' . $row->project . '</td>
                    <td>' . $row->name . '</td>
                    <td>x' . $row->exp . '</td>
                    <td>x' . $row->r2drop . '</td>
                    <td>x' . $row->quest . '</td>
                    <td>x' . $row->silver . '</td>
                    ';
                if (!empty($row->discord)) {
                    echo '
                        <td><a href="' . $row->discord . '" class="btn-floating btn-large pulse grey darken-4 waves-effect waves-light"><i class="fa fa-discord-alt fa-3x"></i></a></td>
                    ';
                } else {
                    echo '
                        <td><a href="" class="btn-floating btn-large pulse red darken-1 waves-effect waves-light disabled"><i class="fa fa-discord-alt fa-3x"></i></a></td>
                    ';
                }
                if (!empty($row->vk)) {
                    echo '
                            <td><a href="' . $row->vk . '" class="btn-floating btn-large pulse grey darken-4 waves-effect waves-light"><i class="fa fa-vk fa-3x"></i></a></td>
                        ';
                } else {
                    echo '
                            <td><a href="" class="btn-floating btn-large pulse red darken-1 waves-effect waves-light disabled"><i class="fa fa-vk fa-3x"></i></a></td>
                        ';
                }

                if (!empty($row->web)) {
                    echo '
                                <td><a href="' . $row->web . '" class="btn-floating btn-large pulse grey darken-4 waves-effect waves-light"><i class="fa fa-internet-explorer fa-3x"></i></a></td>
                            ';
                } else {
                    echo '
                                <td><a href="" class="btn-floating btn-large pulse red darken-1 waves-effect waves-light disabled"><i class="fa fa-internet-explorer fa-3x"></i></a></td>
                               
                            ';
                }

                if (!empty($row->QQ)) {
                    echo '
                                    <td><a href="' . $row->QQ . '" class="btn-floating btn-large pulse grey darken-4 waves-effect waves-light"><i class="fa fa-qq fa-3x"></i></a></td>
                                ';
                } else {
                    echo '
                                    <td><a href="" class="btn-floating btn-large pulse red darken-1 waves-effect waves-light disabled"><i class="fa fa-qq fa-3x"></i></a></td>
                                   
                                ';
                }

            if (!empty($row->telegram)) {
                echo '
                                    <td><a href="' . $row->telegram . '" class="btn-floating btn-large pulse grey darken-4 waves-effect waves-light"><i class="fa fa-telegram fa-3x"></i></a></td>
                                ';
            } else {
                echo '
                                    <td><a href="" class="btn-floating btn-large pulse red darken-1 waves-effect waves-light disabled"><i class="fa fa-telegram fa-3x"></i></a></td>                                   
                                ';
            }
                echo '
                </tr>
                ';
        }
        $query = null;
    }

    function r2mTable() {
        require_once 'admin/bd.php';
        $query = $pdo->query('SELECT `id`, `name`, `class`, `weight`, `description`, `property` FROM `R2M`');
        while ($row = $query->fetch(PDO::FETCH_OBJ)) {
            if ($row->id != "") {
                $img_path = '/img/r2m/'.$row->id.'.jpg';
                $max_width = 48;
                $max_height = 48;
            } else {
                $img_path = "/img/r2m/no-icon.jpg";
                $width = 48;
                $height = 48;
            }

            echo '
                <tr>
                    <td><img src="'.$img_path.'" width="'.$max_width.'" height="'.$max_height.'"></td>
                    <td>' . $row->name . '</td>
                    <td>' . $row->property . '</td>
                    <td>' . $row->class . '</td>
                    <td>' . $row->weight . '</td>
                    <td>' . $row->description . '</td>
                </tr>
                ';
        }
        $query = null;
    }

    function r2mDellEdit() {
        require_once 'admin/bd.php';
        $query = $pdo->query('SELECT `id`, `name`, `class`, `weight`, `description`, `property` FROM `R2M`');
        while ($row = $query->fetch(PDO::FETCH_OBJ)) {
            if ($row->id != "") {
                $img_path = '/img/r2m/'.$row->id.'.jpg';
                $max_width = 48;
                $max_height = 48;
            } else {
                $img_path = "/img/r2m/no-icon.jpg";
            }

            echo '
                <tr class="tr'. $row->id. '">
                    <td><img src="'.$img_path.'" width="'.$max_width.'" height="'.$max_height.'"></td>
                    <td class="tdName">' . $row->name . '</td>
                    <td class="tdProperty">' . $row->property . '</td>
                    <td class="tdClass">' . $row->class . '</td>
                    <td class="tdWeight">' . $row->weight . '</td>
                    <td class="tdDesc">' . $row->description . '</td>
                    <td>
                        <a onclick="deleteData('.$row->id.',\''.$row->name.'\')" class="btn-floating btn-large pulse red darken-1 waves-effect waves-light"><i class="fa fa-trash fa-3x"></i></a>
                        <a onclick="updateData('.$row->id.',\''.$row->name.'\',\''.$row->property.'\',\''.$row->class.'\',\''.$row->weight.'\',\''.$row->description.'\')" class="btn-floating btn-large pulse blue darken-1 waves-effect waves-light"><i class="fa fa-id-card-o fa-3x"></i></a>
                    </td>                      
                </tr>
                ';
        }
        $query = null;
    }
}

?>

