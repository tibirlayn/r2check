<?php
include_once ('includes.php');
$includes = new Includes();
$includes::topIncludes('R2Online Admin');
?>

<!--main-->
<div class="container">
    <H1>R2Online menu admin</H1>

    <ul class="tabs tabs-fixed-width tab-demo z-depth-1">
        <li class="tab"><a class="active" href="#test2">Add</a></li>
        <li class="tab"><a href="#test1">Dell</a></li>
        <li class="tab"><a href="#test3">Edit</a></li>
    </ul>
    <!-- Add project
        <div class="row">
            <form class="col s6" id="form">
                <div class="row">

                    <div class="input-field col s12">
                        <i class="material-icons prefix">pets</i>
                        <input id="drop" name="drop" type="text" class="validate">
                        <label id="drop_error" for="drop">DROP</label>
                        <span class="helper-text"><label id="drop-error" class="error" for="drop"></label></span>
                    </div>

                    <div class="col s12 right-align">
                        <button class="btn waves-effect waves-light light-blue darken-2" id="submit" type="submit" name="reg_submit">Entry
                            <i class="material-icons right">send</i>
                        </button>
                    </div>

                </div>
            </form>
        </div>
        -->
    <!-- ADD -->
    <div id="test2" class="col s12"><p>
        <div class="row">
            <form class="col s6" id="form">
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">account_balance</i>
                        <input id="project" name="project" type="text" class="validate">
                        <label id="project" for="project">Project</label>
                        <span class="helper-text"><label id="project-error" class="error" for="project"></label></span>
                    </div>

                    <div class="input-field col s12">
                        <i class="material-icons prefix">local_library</i>
                        <input id="server_name" name="server_name" type="text" class="validate">
                        <label id="server_name_error" for="server_name">Server Name</label>
                        <span class="helper-text"><label id="server_name-error" class="error" for="server_name"></label></span>
                    </div>

                    <div class="input-field col s12">
                        <i class="material-icons prefix">local_play</i>
                        <input id="exp" name="exp" type="text" class="validate">
                        <label id="exp_error" for="exp">EXP</label>
                        <span class="helper-text"><label id="exp-error" class="error" for="exp"></label></span>
                    </div>

                    <div class="input-field col s12">
                        <i class="material-icons prefix">pets</i>
                        <input id="drop" name="drop" type="text" class="validate">
                        <label id="drop_error" for="drop">DROP</label>
                        <span class="helper-text"><label id="drop-error" class="error" for="drop"></label></span>
                    </div>

                    <div class="input-field col s12">
                        <i class="material-icons prefix">next_week</i>
                        <input id="quest" name="quest" type="text" class="validate">
                        <label id="quest_error" for="quest">QUEST</label>
                        <span class="helper-text"><label id="quest-error" class="error" for="quest"></label></span>
                    </div>

                    <div class="input-field col s12">
                        <i class="material-icons prefix">radio_button_unchecked</i>
                        <input id="silver" name="silver" type="text" class="validate">
                        <label id="silver_error" for="silver">SILVER</label>
                        <span class="helper-text"><label id="silver-error" class="error" for="silver"></label></span>
                    </div>

                    <div class="input-field col s12">
                        <i class="fa fa-discord-alt fa-3x prefix"></i>
                        <input id="discord" name="discord" type="text" class="validate">
                        <label id="discord_error" for="discord">Discord</label>
                        <span class="helper-text"><label id="discord-error" class="error" for="discord"></label></span>
                    </div>

                    <div class="input-field col s12">
                        <i class="fa fa-vk fa-3x prefix"></i>
                        <input id="vk" name="vk" type="text" class="validate">
                        <label id="vk_error" for="vk">VK.COM</label>
                        <span class="helper-text"><label id="vk-error" class="error" for="vk"></label></span>
                    </div>

                    <div class="input-field col s12">
                        <i class="fa fa-internet-explorer fa-3x prefix"></i>
                        <input id="web" name="web" type="text" class="validate">
                        <label id="web_error" for="web">Web site</label>
                        <span class="helper-text"><label id="web-error" class="error" for="web"></label></span>
                    </div>

                    <div class="col s12 right-align">
                        <button class="btn waves-effect waves-light light-blue darken-2" id="submit" type="submit" name="reg_submit">Entry
                            <i class="material-icons right">send</i>
                        </button>
                    </div>

                </div>
            </form>
        </div>
        </p></div>
    <!-- DELL -->
    <div id="test1" class="col s12"><p>
            dell
        </p></div>
    <div id="test3" class="col s12"><p>
            Edit
        </p></div>
</div>

    <script type="text/javascript" src="/js/adminUpdate.js"></script>
    <script type="text/javascript" src="/js/carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.tabs').tabs();

            $("#form").validate({
                rules: {
                    project: {
                        required: true,
                        maxlength: 30
                    },
                    server_name: {
                        required: true,
                        maxlength: 30
                    },
                    exp: {
                        required: true,
                        digits: true,
                        maxlength: 7
                    },
                    drop: {
                        required: true,
                        digits: true,
                        maxlength: 7
                    },
                    quest: {
                        required: true,
                        digits: true,
                        maxlength: 7
                    },
                    silver: {
                        required: true,
                        digits: true,
                        maxlength: 7
                    },
                    discord: {
                        maxlength: 60,
                        url: true
                    },
                    vk: {
                        maxlength: 60,
                        url: true
                    },
                    web: {
                        maxlength: 60,
                        url: true
                    }
                },
                messages: {
                    project: {
                        required: "Поле 'Project' обязательно к заполнению",
                        maxlength: "Введите не больше 30-ти символов в поле 'Project'"
                    },
                    server_name: {
                        required: "Поле 'Server Name' обязательно к заполнению",
                        maxlength: "Введите не больше 30-ти символов в поле 'Server Name'"
                    },
                    exp: {
                        required: "Поле 'Exp' обязательно к заполнению",
                        maxlength: "Введите не больше 7-ми символов в поле 'Exp'",
                        digits:  "Введите только цифры в поле 'Exp'"
                    },
                    drop: {
                        required: "Поле 'Drop' обязательно к заполнению",
                        maxlength: "Введите не больше 7-ми символов в поле 'Drop'",
                        digits:  "Введите только цифры в поле 'Drop'"
                    },
                    quest: {
                        required: "Поле 'Quest' обязательно к заполнению",
                        maxlength: "Введите не больше 7-ми символов в поле 'Quest'",
                        digits:  "Введите только цифры в поле 'Quest'"
                    },
                    silver: {
                        required: "Поле 'Silver' обязательно к заполнению",
                        maxlength: "Введите не больше 7-ми символов в поле 'Silver'",
                        digits:  "Введите только цифры в поле 'Silver'"
                    },
                    discord: {
                        maxlength: "Введите не больше 60-ти символов в поле 'Discord'",
                        url: "Не правильно введена ссылка на канал Discord."
                    },
                    vk: {
                        maxlength: "Введите не больше 60-ти символов в поле 'VK.COM'",
                        url: "Не правильно введена ссылка на канал VK."
                    },
                    web: {
                        maxlength: "Введите не больше 60-ти символов в поле 'Web site'",
                        url: "Не правильно введена ссылка на канал Web site."
                    }
                }
            });

            

        });
    </script>

<?php
$includes::bot();
?>
