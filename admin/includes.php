<?php
class Includes {
    function topIncludes($title) {
        echo '
            <!DOCTYPE html>
            <html>
            <head>
                <!--Import Google Icon Font-->
                <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
                <!--Import materialize.css-->
                <link type="text/css" rel="stylesheet" href="/css/materialize.css"  media="screen,projection"/>
                
                <link rel="stylesheet" href="/css/fork-awesome.min.css">
                <link rel="stylesheet" href="/css/style.css">
            
                <link rel="shortcut icon" href="/img/main/main.jpg" type="image/jpeg">
                <!--Let browser know website is optimized for mobile-->
                <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0"/>
                
                <!-- <meta charset="utf-8"> -->
                <title>'.$title.'</title>
            
                <script type="text/javascript" src="/js/jquery-3.2.1.js"></script>
                <script type="text/javascript" src="/js/materialize.min.js"></script>
                <script type="text/javascript" src="https://vk.com/js/api/openapi.js?168"></script>
                
            </head>
            <body>
            <nav>
                <div class="nav-wrapper grey darken-4">
                    <a href="https://r2check.ru/" class="brand-logo center">R2Online</a>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="https://r2check.ru/reg"><i class="material-icons">view_module</i></a></li>
                        <li><a href="https://r2check.ru/donation"><i class="material-icons">monetization_on</i></a></li>
                        <li><a href="https://r2check.ru/guarant"><i class="material-icons">gavel</i></a></li>
                        <li><a href="https://r2check.ru/r2m"><h5><b>R2M</b></h5></a></li>
                      </ul>
                </div>
            </nav>
    ';
    }
    function bot() {
        echo '
        <!-- VK Widget -->
        <div id="vk_community_messages"></div>
        <script type="text/javascript">
            VK.Widgets.CommunityMessages("vk_community_messages", 168663909, {expandTimeout: "10000000",tooltipButtonText: "Есть вопрос?"});
        </script>
        </div>
        <footer class="page-footer grey darken-3">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">System Corporation ING</h5>
                <p class="grey-text text-lighten-4">
                Молодая компания, которая ищет талантливых людей в свою команду. Особенно будем рады художникам, программистам.<br>
                Наша команда создала этот сайт для ознакомления. Подробную информацию о правилах данного сайта вы можете посмотреть  
                
                <a href="https://r2check.ru/reg">здесь.</a>
                </p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a href="https://vk.com/r2main">VKontakte (R2Main)</a></li>
                  <li><a href="https://vk.com/r2mobile">VKontakte (R2Mobile)</a></li>
                  <li><a href="https://vk.me/join/JmZ3xV5AJ0VRucW__yjiqk2LIaEX012n3QI=">VKontakte Chat</a></li>
                  <li><a href="https://discord.gg/nBjKvxw">Discord</a></li>
                  <li><a href="https://t.me/r2main">Telegram</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright grey darken-4">
            <div class="container">
            <p>Copyright © 2020 <a href="https://vk.com/r2main">R2Online Main News</a>
            <a class="right" href="https://vk.com/tibirlayn">artemiy1</a>
            </p>
            </div>
          </div>
        </footer>
        </body>
        </html>
    ';
    }
    function topError($title) {
        echo '
            <!DOCTYPE html>
            <html>
            <head>
                <link rel="stylesheet" href="/css/error.css">        
                <link rel="shortcut icon" href="/img/main/main.jpg" type="image/jpeg">
                <!--Let browser know website is optimized for mobile-->
                <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0"/> 
                <!-- <meta charset="utf-8"> -->

                <title>'.$title.'</title>
            </head>
            <body>
    ';
    }
    function botError() {
        echo '
        <script type="text/javascript" src="/js/particles.js"></script>
        </body>
        </html>
    ';
    }
}
?>