<?php
if ($_SERVER['HTTP_REFERER'] == "https://r2check.ru/adminr2m" && $_SERVER['REQUEST_URI'] == "/adminset" && $_SERVER['PHP_SELF'] == "/index.php") {
    if (isset($_POST['addFormDate']) && $_POST['addFormDate'] == 'Add') {
        $jsonPost = new JsonPost();
        $jsonPost::r2mPostAdd();
    } else if (isset($_POST['dellFormDate']) && $_POST['dellFormDate'] == 'Dell') {
        $jsonPost = new JsonPost();
        $jsonPost::r2mPostDell();
    } else if (isset($_POST['editFormDate']) && $_POST['editFormDate'] == 'Edit') {
        $jsonPost = new JsonPost();
        $jsonPost::r2mPostUp();
    }
} else {
    echo json_encode(["status"=>"error", "error"=>'Ошибка']);
}
class JsonPost {
    function r2mPostAdd() {
        if (isset($_FILES['fileName'])) {
            // Получаем нужные элементы массива "fileName"
            $fileName = $_FILES['fileName']['name'];
            $fileNameTypy = $_FILES['fileName']['type'];
            $fileNameSize = $_FILES['fileName']['size'];
            $fileNameTMP = $_FILES['fileName']['tmp_name'];
            $fileNameError = $_FILES['fileName']['error'];
            $errors = array();

            // Проверим на ошибки
            if ($fileNameError !== UPLOAD_ERR_OK || !is_uploaded_file($fileNameTMP)) {
                // Массив с названиями ошибок
                $errorMessages = [
                    UPLOAD_ERR_INI_SIZE   => 'Размер файла превысил значение upload_max_filesize в конфигурации PHP.',
                    UPLOAD_ERR_FORM_SIZE  => 'Размер загружаемого файла превысил значение MAX_FILE_SIZE в HTML-форме.',
                    UPLOAD_ERR_PARTIAL    => 'Загружаемый файл был получен только частично.',
                    UPLOAD_ERR_NO_FILE    => 'Файл не был загружен.',
                    UPLOAD_ERR_NO_TMP_DIR => 'Отсутствует временная папка.',
                    UPLOAD_ERR_CANT_WRITE => 'Не удалось записать файл на диск.',
                    UPLOAD_ERR_EXTENSION  => 'PHP-расширение остановило загрузку файла.',
                ];
                // Зададим неизвестную ошибку
                $unknownMessage = 'При загрузке файла произошла неизвестная ошибка.';
                // Если в массиве нет кода ошибки, скажем, что ошибка неизвестна
                $errors[] = isset($errorMessages[$fileNameError]) ? $errorMessages[$fileNameError] : $unknownMessage;
            } else {
                if ($fileName != "" || $fileNameTypy == "image/jpeg" || $fileNameSize > 0 || $fileNameSize < 200000 || $fileNameError == 0) {
                    //определить разршение файла 48x48
                    $getImage = getimagesize($fileNameTMP);
                    $widthImage = $getImage[0];
                    $heightImage = $getImage[1];
                    if ($widthImage == 48 && $heightImage == 48) {
                        // Создадим ресурс FileInfo
                        $fi = finfo_open(FILEINFO_MIME_TYPE);
                        // Получим MIME-тип
                        $mime = (string) finfo_file($fi, $fileNameTMP);
                        // Проверим ключевое слово image (image/jpeg, image/png и т. д.)
                        if (strpos($mime, 'image') === false) {
                            $errors[] = 'Можно загружать только изображения.';
                        }
                        $name = trim($_POST['name']);
                        $property = trim($_POST['property']);
                        $classname = trim($_POST['classname']);
                        $weight = trim($_POST['weight']);
                        $description = trim($_POST['description']);
                        if (empty($name) || empty($property) || empty($weight) || empty($description) || empty($classname)) {
                            $errors[] = 'Заполните все поля';
                        }
                    } else {
                        $errors[] = 'Картинка должна быть 48х48! Ваша картинка: ширина = ' . $widthImage . ' высота = ' . $heightImage;
                    }
                } else {
                    $errors[] = 'Нет файла или картинка не в формате .jpg';
                }
            }
        } else {
            $errors[] = 'Нет файла или картинка не в формате .jpg';
        }

        if (empty($errors)) {
            //взять id с базы и назначить новое название файлу
             require_once 'admin/bd.php';
             $query = $pdo;
             $sql = "INSERT INTO R2M (name, description, class, weight, property) VALUES (?,?,?,?,?)";
             $stmt= $query->prepare($sql);
             $stmt->execute([$name, $description, $classname, $weight, $property]);
             $id = $query->lastInsertId();
             $query = null;

            //записать файл на сервер
            // Сгенерируем новое имя файла на основе MD5-хеша
            $name = $id;
            // Сгенерируем расширение файла на основе типа картинки
            $extension = image_type_to_extension($getImage[2]);
            // Сократим .jpeg до .jpg
            $format = str_replace('jpeg', 'jpg', $extension);

            // Переместим картинку с новым именем и расширением в папку /img/r2m/
            if (!move_uploaded_file($fileNameTMP, '/home/b/bykovk5c/r2check.ru/public_html/img/r2m/' . $name . $format)) {
                $errors[] = 'При записи изображения на диск произошла ошибка.';
                echo json_encode(["status"=>"error", "error"=>$errors]);
            } else {
                echo json_encode(["status"=>"success"]);
            }
        } else {
            echo json_encode(["status"=>"error", "error"=>$errors]);
        }
    }

    function r2mPostDell() {
        $id = trim($_POST['id']);
        $errors[] = array();
        if (!empty($id)) {
            require_once 'admin/bd.php';
            $stmt = $pdo->prepare('DELETE FROM `R2M` WHERE `id`=?');
            $stmt->bindParam(1, $id);
            if ($stmt->execute()) {
                echo json_encode(["status"=>"success", "success"=>$id]);
            } else {
                $errors[] = "Ошибка, нет данных id!";
                echo json_encode(["status"=>"error", "error"=>$errors]);
            }
        } else {
            $errors[] = "Ошибка, нет данных id!";
            echo json_encode(["status"=>"error", "error"=>$errors]);
        }
    }

    function r2mPostUp() {
        $errors = array();
        $fileName = $_FILES['fileName_up']['name'];
        $fileNameTypy = $_FILES['fileName_up']['type'];
        $fileNameSize = $_FILES['fileName_up']['size'];
        $fileNameTMP = $_FILES['fileName_up']['tmp_name'];
        $fileNameError = $_FILES['fileName_up']['error'];

        $name_up = trim($_POST['name_up']);
        $property_up = trim($_POST['property_up']);
        $classname_up = trim($_POST['classname_up']);
        $weight_up = trim($_POST['weight_up']);
        $description_up = trim($_POST['description_up']);
        $id = trim($_POST['id']);

        if (!empty($fileName)) {
            if ($fileNameError !== UPLOAD_ERR_OK || !is_uploaded_file($fileNameTMP)) {
                // Массив с названиями ошибок
                $errorMessages = [
                    UPLOAD_ERR_INI_SIZE   => 'Размер файла превысил значение upload_max_filesize в конфигурации PHP.',
                    UPLOAD_ERR_FORM_SIZE  => 'Размер загружаемого файла превысил значение MAX_FILE_SIZE в HTML-форме.',
                    UPLOAD_ERR_PARTIAL    => 'Загружаемый файл был получен только частично.',
                    UPLOAD_ERR_NO_FILE    => 'Файл не был загружен.',
                    UPLOAD_ERR_NO_TMP_DIR => 'Отсутствует временная папка.',
                    UPLOAD_ERR_CANT_WRITE => 'Не удалось записать файл на диск.',
                    UPLOAD_ERR_EXTENSION  => 'PHP-расширение остановило загрузку файла.',
                ];
                // Зададим неизвестную ошибку
                $unknownMessage = 'При загрузке файла произошла неизвестная ошибка.';
                // Если в массиве нет кода ошибки, скажем, что ошибка неизвестна
                $errors[] = isset($errorMessages[$fileNameError]) ? $errorMessages[$fileNameError] : $unknownMessage;
            } else {
                if ($fileName != "" || $fileNameTypy == "image/jpeg" || $fileNameSize > 0 || $fileNameSize < 200000 || $fileNameError == 0) {
                    //определить разршение файла 48x48
                    $getImage = getimagesize($fileNameTMP);
                    $widthImage = $getImage[0];
                    $heightImage = $getImage[1];
                    if ($widthImage == 48 && $heightImage == 48) {
                        // Создадим ресурс FileInfo
                        $fi = finfo_open(FILEINFO_MIME_TYPE);
                        // Получим MIME-тип
                        $mime = (string)finfo_file($fi, $fileNameTMP);
                        // Проверим ключевое слово image (image/jpeg, image/png и т. д.)
                        if (strpos($mime, 'image') === false) {
                            $errors[] = 'Можно загружать только изображения.';
                        }
                    } else {
                        $errors[] = 'Картинка должна быть 48х48! Ваша картинка: ширина = ' . $widthImage . ' высота = ' . $heightImage;
                    }
                } else {
                    $errors[] = 'Картинка большого размера! Данные не добавлены';
                }
            }
        }

        if (empty($errors)) {
            require_once 'admin/bd.php';
            $query = $pdo;
            $sql = "UPDATE R2M SET name=?, description=?, class=?, weight=?, property=? WHERE id=?";
            $stmt= $query->prepare($sql);
            $stmt->execute([$name_up, $property_up, $classname_up, $weight_up, $description_up, $id]);
            $query = null;

            if (!empty($fileName)) {
                // Сгенерируем новое имя файла на основе MD5-хеша
                $name = $id;
                // Сгенерируем расширение файла на основе типа картинки
                $extension = image_type_to_extension($getImage[2]);
                // Сократим .jpeg до .jpg
                $format = str_replace('jpeg', 'jpg', $extension);
                // Удалить файл
                unlink("../img/r2m/" . $name . $format);
                // Переместим картинку с новым именем и расширением в папку /img/r2m/
                if (!move_uploaded_file($fileNameTMP, '/home/b/bykovk5c/r2check.ru/public_html/img/r2m/' . $name . $format)) {
                    $errors[] = 'При записи изображения на диск произошла ошибка.';
                    echo json_encode(["status"=>"error", "error"=>$errors]);
                } else {
                    echo json_encode(["status"=>"success"]);
                }
            } else {
                echo json_encode(["status"=>"success"]);
            }
        } else {
            echo json_encode(["status"=>"error", "error"=>$errors]);
        }
    }

}
?>