<?php
include_once ('includes.php');
$includes = new Includes();
$includes::topIncludes('R2Mobile Admin');
?>
<!--main-->
<div class="container">
    <H1>R2Online menu admin</H1>
    <ul class="tabs tabs-fixed-width tab-demo z-depth-1">
        <li class="tab"><a class="active" href="#test2">Add</a></li>
        <li class="tab"><a href="#test1">Dell</a></li>
        <li class="tab"><a href="#test3">Avatar</a></li>
    </ul>
    <div id="test2" class="col s12"><p>
        <div class="row">
            <form class="col s6" id="formAdd">
                <div class="row">
                    <!--Image-->
                    <div class="file-field input-field">
                        <div class="btn">
                            <span>File</span>
                            <input type="file" name="fileName" id="fileName">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" id="file_name_r2m" name="file_name_r2m" type="text" placeholder="Upload one file">
                            <span class="helper-text"><label id="file_name_r2m-error" class="error" for="file_name_r2m"></label></span>
                        </div>
                    </div>
                    <!--Name-->
                    <div class="input-field col s12">
                        <i class="material-icons prefix">local_library</i>
                        <input id="name" name="name" type="text" class="validate">
                        <label id="name_error" for="name">Name</label>
                        <span class="helper-text"><label id="name-error" class="error" for="name"></label></span>
                    </div>
                    <!--Property-->
                    <div class="input-field col s12">
                        <i class="material-icons prefix">local_play</i>
                        <input id="property" name="property" type="text" class="validate">
                        <label id="property_error" for="property">Property</label>
                        <span class="helper-text"><label id="property-error" class="error" for="property"></label></span>
                    </div>
                    <!--Class-->
                    <div class="input-field col s12">
                        <i class="material-icons prefix">pets</i>
                        <input id="classname" name="classname" type="text" value="" class="validate">
                        <label id="classname_error" for="classname">Class</label>
                        <span class="helper-text"><label id="classname-error" class="error" for="classname"></label></span>
                    </div>
                    <!--Weight-->
                    <div class="input-field col s12">
                        <i class="material-icons prefix">next_week</i>
                        <input id="weight" name="weight" type="text" class="validate">
                        <label id="weight_error" for="weight">Weight</label>
                        <span class="helper-text"><label id="weight-error" class="error" for="weight"></label></span>
                    </div>
                    <!--Description-->
                    <div class="input-field col s12">
                        <i class="material-icons prefix">radio_button_unchecked</i>
                        <textarea id="description" name="description"  class="materialize-textarea validate" data-length="500"></textarea>
                        <label id="description_error" for="description">Description</label>
                        <span class="helper-text"><label id="description-error" class="error" for="description"></label></span>
                    </div>

                    <div class="col s12 right-align">
                        <button class="btn waves-effect waves-light light-blue darken-2" id="add_submit" type="submit" name="add_submit">Entry
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        </p></div>
    <!-- DELL -->
    <div id="test1" class="col s12">
        <nav class="nav-margin-top25">
            <div class="nav-wrapper purple darken-2">
                <div class="row">
                    <div class="col s3">
                        <div class="input-field">
                            <input id="search" type="search" required>
                            <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                            <i class="material-icons">close</i>
                        </div>
                    </div>
                    <div class="col s9">
                        <ul class="right hide-on-med-and-down"
                        <!-- Dropdown Trigger -->
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Доспехи<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Оружие<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Аксессуары<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Материалы<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Сферы<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Монстры<i class="material-icons right">arrow_drop_down</i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <table class="striped centered" id="tableUpadte">
            <thead>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Property</th>
                <th>Class</th>
                <th>Weight</th>
                <th>Description</th>
                <th>Actions (Del\Edit)</th>
            </tr>
            </thead>
            <tbody id="tableDelEdit">
            <?php
            require_once 'admin/bdsec.php';
            $table = new Table();
            $table::r2mDellEdit();
            ?>
            </tbody>
        </table>
    </div>
    <!-- Edit -->
    <div id="test3" class="col s12"><p>
            Avatar
        </p></div>
</div>
    <!-- Модальное окно -->
    <div id="modalEdit" class="modal">
        <div class="modal-content">
            <h4>Изменить данные</h4>
            <form class="col s6" id="formUpdate" name="formUpdate">
                <!--Image-->
                <div class="file-field input-field">
                    <div class="btn">
                        <span>File</span>
                        <input type="file" name="fileName_up" id="fileName_up">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" id="file_name_r2m_up" name="file_name_r2m_up" type="text" placeholder="Upload one file">
                        <span class="helper-text"><label id="file_name_r2m_up-error" class="error" for="file_name_r2m_up"></label></span>
                    </div>
                </div>
                <!--Name-->
                <div class="input-field col s12">
                    <i class="material-icons prefix">local_library</i>
                    <input id="name_up" name="name_up" value=" " type="text" class="validate">
                    <label id="name_up_error" for="name_up">Name</label>
                    <span class="helper-text"><label id="name_up-error" class="error" for="name_up"></label></span>
                </div>
                <!--Property-->
                <div class="input-field col s12">
                    <i class="material-icons prefix">local_play</i>
                    <input id="property_up" name="property_up" value=" " type="text" class="validate">
                    <label id="property_up_error" for="property_up">Property</label>
                    <span class="helper-text"><label id="property_up-error" class="error" for="property_up"></label></span>
                </div>
                <!--Class-->
                <div class="input-field col s12">
                    <i class="material-icons prefix">pets</i>
                    <input id="classname_up" name="classname_up" value=" " type="text" class="validate">
                    <label id="classname_up_error" for="classname_up">Class</label>
                    <span class="helper-text"><label id="classname_up-error" class="error" for="classname_up"></label></span>
                </div>
                <!--Weight-->
                <div class="input-field col s12">
                    <i class="material-icons prefix">next_week</i>
                    <input id="weight_up" name="weight_up" type="text" value=" " class="validate">
                    <label id="weight_up_error" for="weight_up">Weight</label>
                    <span class="helper-text"><label id="weight_up-error" class="error" for="weight_up"></label></span>
                </div>
                <!--Description-->
                <div class="input-field col s12">
                    <i class="material-icons prefix">radio_button_unchecked</i>
                    <textarea id="description_up" name="description_up"  class="materialize-textarea validate" data-length="500"> </textarea>
                    <label id="description_up_error" for="description_up">Description</label>
                    <span class="helper-text"><label id="description_up-error" class="error" for="description_up"></label></span>
                </div>
                <div class="modal-footer">
                    <a class="modal-close waves-effect waves-green btn-flat">Не согласен</a>
                    <button class="btn waves-effect waves-light light-blue darken-2" id="submitUpdate" type="submit" name="add_submit">Entry
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript" src="/js/adminUpdate.js"></script>
    <script type="text/javascript" src="/js/adminSetting.js"></script>
    <script type="text/javascript" src="/js/carousel.js"></script>
    <script type="text/javascript" src="/js/material-dialog.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.modal').modal();
        $('.tabs').tabs();
        $('textarea#description').characterCounter();

        $("#formAdd").validate({
            rules: {
                file_name_r2m: {
                    required: true,
                    maxlength: 10,
                    minlength: 1,
                    extension: "jpg"
                },
                name: {
                    required: true,
                    maxlength: 50,
                    minlength: 3
                },
                property: {
                    required: true,
                    maxlength: 50,
                    minlength: 3
                },
                classname: {
                    required: true,
                    digits: true,
                    maxlength: 1,
                    minlength: 1
                },
                weight: {
                    required: true,
                    digits: true,
                    maxlength: 5,
                    minlength: 1
                },
                description: {
                    required: true,
                    maxlength: 500,
                    minlength: 3
                }
            },
            messages: {
                file_name_r2m: {
                    required: "Поле 'File' обязательно к заполнению",
                    maxlength: "Введите не больше 10-ти символов в поле 'File'",
                    minlength: "Введите не меньше 1-го символов в поле 'File'",
                    extension: "Поле 'File' обязательно в формате .jpg"
                },
                name: {
                    required: "Поле 'Name' обязательно к заполнению",
                    maxlength: "Введите не больше 50-ти символов в поле 'Name'",
                    minlength: "Введите не меньше 3-го символов в поле 'Name'"
                },
                property: {
                    required: "Поле 'Property' обязательно к заполнению",
                    maxlength: "Введите не больше 50-ми символов в поле 'Property'",
                    minlength: "Введите не меньше 3-го символов в поле 'Property'"
                },
                classname: {
                    required: "Поле 'Class' обязательно к заполнению",
                    maxlength: "Введите не больше 1-ми символов в поле 'Class'",
                    minlength: "Введите не меньше 1-го символов в поле 'Class'",
                    digits:  "Введите только цифры в поле 'Class'"
                },
                weight: {
                    required: "Поле 'Weight' обязательно к заполнению",
                    maxlength: "Введите не больше 5-го символов в поле 'Weight'",
                    minlength: "Введите не меньше 1-го символов в поле 'Weight'",
                    digits:  "Введите только цифры в поле 'Weight'"
                },
                description: {
                    required: "Поле 'Description' обязательно к заполнению",
                    maxlength: "Введите не больше 500-от символов в поле 'Description'",
                    minlength: "Введите не меньше 3-ех символов в поле 'Description'"
                }
            }
        });

            $('#formUpdate').validate({
                rules: {
                    file_name_r2m_up: {
                        required: false,
                        maxlength: 10,
                        minlength: 1,
                        extension: "jpg"
                    },
                    name_up: {
                        required: false,
                        maxlength: 50,
                        minlength: 3
                    },
                    property_up: {
                        required: false,
                        maxlength: 50,
                        minlength: 3
                    },
                    classname_up: {
                        required: false,
                        digits: true,
                        maxlength: 1,
                        minlength: 1
                    },
                    weight_up: {
                        required: false,
                        digits: true,
                        maxlength: 5,
                        minlength: 1
                    },
                    description_up: {
                        required: false,
                        maxlength: 500,
                        minlength: 3
                    }
                },
                messages: {
                    file_name_r2m_up: {
                        required: "Поле 'File' обязательно к заполнению",
                        maxlength: "Введите не больше 10-ти символов в поле 'File'",
                        minlength: "Введите не меньше 1-го символов в поле 'File'",
                        extension: "Поле 'File' обязательно в формате .jpg"
                    },
                    name_up: {
                        required: "Поле 'Name' обязательно к заполнению",
                        maxlength: "Введите не больше 50-ти символов в поле 'Name'",
                        minlength: "Введите не меньше 3-го символов в поле 'Name'"
                    },
                    property_up: {
                        required: "Поле 'Property' обязательно к заполнению",
                        maxlength: "Введите не больше 50-ми символов в поле 'Property'",
                        minlength: "Введите не меньше 3-го символов в поле 'Property'"
                    },
                    classname_up: {
                        required: "Поле 'Class' обязательно к заполнению",
                        maxlength: "Введите не больше 1-ми символов в поле 'Class'",
                        minlength: "Введите не меньше 1-го символов в поле 'Class'",
                        digits:  "Введите только цифры в поле 'Class'"
                    },
                    weight_up: {
                        required: "Поле 'Weight' обязательно к заполнению",
                        maxlength: "Введите не больше 5-го символов в поле 'Weight'",
                        minlength: "Введите не меньше 1-го символов в поле 'Weight'",
                        digits:  "Введите только цифры в поле 'Weight'"
                    },
                    description_up: {
                        required: "Поле 'Description' обязательно к заполнению",
                        maxlength: "Введите не больше 500-от символов в поле 'Description'",
                        minlength: "Введите не меньше 3-ех символов в поле 'Description'"
                    }
                }
            });

    });
</script>

<?php
$includes::bot();
?>