function updateData(strId, strName, strProperty, strClass, strWeight, strDescription) {
    var id = strId, name = strName, property = strProperty, classname = strClass, weight = strWeight, description = strDescription, trId = "tr" + strId;
    if (id != "" && name != "" && property != "" && classname != "" && weight != "" && description != "") {
        const elem = document.getElementById('modalEdit');
        const instance = M.Modal.init(elem, {dismissible: false});
        instance.open();
        $('input[name="name_up"]').val(name);
        $('input[name="property_up"]').val(property);
        $('input[name="classname_up"]').val(classname);
        $('input[name="weight_up"]').val(weight);
        $('textarea[name="description_up"]').val(description);

        $('#formUpdate').submit(function(){
            if($("#formUpdate").valid()){
                var formDate = new FormData(this);
                formDate.append('editFormDate', 'Edit');
                formDate.append('id', id);
                for (var pair of formDate.entries()) {
                    console.log(pair[0]+ ', '+ pair[1]);
                }
                $.ajax({
                    type: "POST",
                    url: "adminset",
                    data: formDate,
                    contentType: false,
                    processData:false,
                    cache: false
                }).done(function(data) {
                    var response = JSON.parse(data);
                    if(response.status === 'error') {
                        //alert(response.error);
                        M.toast({html: response.error});
                    } else if (response.status === 'success') {
                        //alert(response.success);
                        $('#modalEdit').modal('close');
                        M.toast({html: 'Данные изменены!'});
                        $('#tableUpadte').dataTable();
                    }
                });
                return false;
            }
        });

    } else {
        M.toast({html: 'Системная ошибка!'});
    }
    return false;
}