$("#formAdd").submit(function(){
    if($("#formAdd").valid()){
        var selectedFile = $('#fileName')[0].files[0];
        if (selectedFile.name == "" || selectedFile.type != "image/jpeg" || selectedFile.size == 0) {
            M.toast({html: 'Нет файла или файл не правильного формата!'});
            /*alert(selectedFile.name);
            console.log(selectedFile.type);
            alert(selectedFile.size);*/
        } else if (selectedFile.name != "" && selectedFile.name.length <= 10 && selectedFile.type == "image/jpeg" && selectedFile.size > 0 && selectedFile.size < 200000) {
            var formDate = new FormData(this);
            formDate.append('addFormDate', 'Add');
            $.ajax({
                type: "POST",
                url: "adminset",
                data: formDate,
                contentType: false,
                processData:false,
                cache: false
            }).done(function(data) {
                var response = JSON.parse(data);
                if(response.status === 'error') {
                    //alert(response.error);
                    M.toast({html: response.error});
                } else if (response.status === 'success') {
                    //alert(response.success);
                    document.getElementById('file_name_r2m').value = "";
                    document.getElementById('name').value = "";
                    document.getElementById('property').value = "";
                    document.getElementById('classname').value = "";
                    document.getElementById('weight').value = "";
                    document.getElementById('description').value = "";
                    M.toast({html: 'Данные добавлены!'});
                }
            });
        } else {
            M.toast({html: 'Картинка большого размера! Данные не добавлены'});
        }
        return false;
    }
});

function deleteData(strId, strName) {
    var id = strId, name = strName, trId = "tr" + strId;
    if (id != "" && name != "") {
        MaterialDialog.dialog(
            'Вы дествительно хотите удалить данные ' + name + '?',
            {
                title:'Удалить данные',
                buttons:{ 
                    close:{
                        className:"red",
                        text:"Disagree",
                        callback:function(){
                        }
                    },
                    confirm:{
                        className:"blue",
                        text:"Agree",
                        callback:function(){
                            if (Number.isInteger(id)) {
                                var dellFormDate = 'Dell';
                                $.ajax({
                                    type: "POST",
                                    url: "adminset",
                                    data: "id=" + id + "&dellFormDate=" + dellFormDate
                                }).done(function (data) {
                                    var response = JSON.parse(data);
                                    if (response.status === 'error') {
                                        M.toast({html: response.error});
                                    } else if (response.status === 'success') {
                                        $('.' + trId).fadeOut().remove("slow");
                                        M.toast({html: 'Данные удалены!'});
                                    } else {
                                        M.toast({html: 'Системная ошибка!'});
                                    }
                                });
                            } else {
                                M.toast({html: 'Системная ошибка!'});
                            }
                        }
                    }
                }
            }
        );
    } else {
        M.toast({html: 'Системная ошибка!'});
    }
    return false;
}