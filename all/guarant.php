<?php
include_once ('admin/includes.php');
$includes = new Includes();
$includes::topIncludes('Guarantor');
?>

    <div class="container">
        <h1>Гарант (Guarantor)</h1>
        <p class="flow-text" align="justify">
            Предоставляем услуги гаранта, которые помогут осуществить уверенную, надежную и безопасную сделку между кем-либо.<br>
            В услуги гаранта входят следующие:
        </p>
                <li class="flow-text" align="justify">Купля\Продажа игровых персонажей;</li>
                <li class="flow-text" align="justify">Купля\Продажа игровой валюты (Серебро);</li>
                <li class="flow-text" align="justify">Обмен между серверами и игровых ценностей, в том числе другие сделки.</li>
        <p class="flow-text" align="justify">
             Стоимость любой эскорт услуги не зависимо от суммы (серебра) или уровня персонажа составляет 70 рублей.<br>
            Оплатить услугу можно следующим способом QIWI, СБЕР Онлайн.<br>
            Связаться с нами можно <br>
            Артур Кристибиров <a href="https://vk.com/tibirlayn" class="btn-floating btn-large pulse grey darken-4 waves-effect waves-light"><i class="fa fa-vk fa-3x"></i></a> <br>
            Андрей Федотов <a href="https://vk.com/id259811900" class="btn-floating btn-large pulse grey darken-4 waves-effect waves-light"><i class="fa fa-vk fa-3x"></i></a>

        </p>
    </div>

<?php
$includes::bot();
?>