<?php
include_once ('admin/includes.php');
$includes = new Includes();
$includes::topIncludes('R2Online');
?>

<!--slider
<div class="slider hide-on-med-and-down">
    <ul class="slides">
        <li>
            <img src="/img/slider/slider6.jpg">
            <div class="caption right-align">
                <h3 class="slider_h3">R2 Online Main News</h3>
                <h5 class="slider_h3">vk.com/r2main</h5>
            </div>
        </li>
        <li>
            <img src="/img/slider/slider1.jpg">
            <div class="caption center-align">
                <h3 class="slider_h3">R2 Online</h3>
                <h5 class="slider_h3">Игра по твоим правилам.</h5>
            </div>
        </li>
        <li>
            <img src="/img/slider/slider2.jpg">
            <div class="caption left-align">
                <h3 class="slider_h3">R2 Online</h3>
                <h5 class="slider_h3">Free place.</h5>
            </div>
        </li>
        <li>
            <img src="/img/slider/slider3.jpg">
            <div class="caption left-align">
                <h3 class="slider_h3">R2 Online</h3>
                <h5 class="slider_h3">Играй и побеждай красиво.</h5>
            </div>
        </li>
        <li>
            <img src="/img/slider/slider4.jpg">
            <div class="caption right-align">
                <h3 class="slider_h3">R2 Online</h3>
                <h5 class="slider_h3">Разделяй и властвуй. (Свободное место.)</h5>
            </div>
        </li>
        <li>
            <img src="/img/slider/slider5.jpg"> 
            <div class="caption left-align">
                <h3 class="slider_h3">R2 Online</h3>
                <h5 class="slider_h3">No Rules, Just Power!</h5>
            </div>
        </li>
    </ul>
</div>
main-->
<div class="container">
    <H1>R2Online Server</H1>
    <table class="striped centered highlight">
        <thead>
        <tr>
            <th>Project</th>
            <th>Server Name</th>
            <th>EXP</th>
            <th>DROP</th>
            <th>QUEST</th>
            <th>SILVER</th>
            <th>Discord</th>
            <th>VK.COM</th>
            <th>Website</th>
            <th>QQ</th>
            <th>Telegram</th>
        </tr>
        </thead>

        <tbody>
        <?php
            require_once 'admin/bdsec.php';
            $table = new Table();
            $table::mainTable();
        ?>
        </tbody>
    </table>

    <script type="text/javascript" src="/js/carousel.js"></script>

<?php
$includes::bot();
?>
