<?php
include_once ('admin/includes.php');
$includes = new Includes();
$includes::topIncludes('R2M');
?>
    <div class="container">
        <H1>R2Mobile</H1>

        <ul id="dropdown1" class="dropdown-content">
            <li><a href="#!">one</a></li>
            <li><a href="#!">two</a></li>
            <li class="divider"></li>
            <li><a href="#!">three</a></li>
        </ul>
        <nav>
            <div class="nav-wrapper purple darken-2">
                <div class="row">
                    <div class="col s3">
                        <div class="input-field">
                            <input id="search" type="search" required>
                            <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                            <i class="material-icons">close</i>
                        </div>
                    </div>
                    <div class="col s9">
                        <ul class="right hide-on-med-and-down"
                        <!-- Dropdown Trigger -->
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Доспехи<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Оружие<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Аксессуары<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Материалы<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Сферы<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-trigger" href="" data-target="dropdown1">Монстры<i class="material-icons right">arrow_drop_down</i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <table class="striped centered ">
            <thead>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Property</th>
                <th>Class</th>
                <th>Weight</th>
                <th>Description</th>

            </tr>
            </thead>

            <tbody>
            <?php
                require_once 'admin/bdsec.php';
                $table = new Table();
                $table::r2mTable();
            ?>
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        $(".dropdown-trigger").dropdown();
    </script>
<?php
$includes::bot();
?>


